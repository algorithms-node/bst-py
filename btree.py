class BtreeNode:
    '''
   Btree node class
   '''
    def __init__(self, data):
        self.data = data
        self.left = None
        self.right = None

class Btree:
    '''
   Btree Class
   '''
    def __init__(self):
        self.root = None

    def insertNode(self, parent, node):
        if parent.data > node.data:
            if parent.left is None:
                parent.left = node
            else:
                self.insertNode(parent.left, node)
        else:
            if parent.right is None:
                parent.right = node
            else:
                self.insertNode(parent.right, node)

    def delete(self, nodeData):
        if self.root is None:
            return None
        self.deleteNode(self.root, nodeData)

    def deleteNode(self, node, nodeData):
        if nodeData < node.data:
            node.left = self.deleteNode(node.left, nodeData)
        elif nodeData > node.data:
            node.right = self.deleteNode(node.right, nodeData)
        else:
            if node.left is None and node.right is None:
                node = None
                return node
            if node.left is None:
                node = node.right
                return node
            if node.right is None:
                node =node.left
                return node
            minRightSubTree = self.findMin(node.right)
            node.data = minRightSubTree.data
            node.right = self.deleteNode(node.right, minRightSubTree.data)
            return node

    def findMin(self, node):
        if node.left is None:
            return node
        else:
            return self.findMin(node.left)

    def insert(self, data):
        newNode = BtreeNode(data)
        if self.root is None:
            self.root = newNode
        else:
            self.insertNode(self.root, newNode)

    def inorder(self, root):
        if root is not None:
            self.inorder(root.left)
            print(root.data)
            self.inorder(root.right)

    def preorder(self, root):
        if root is not None:
            print(root.data)
            self.preorder(root.left)
            self.preorder(root.right)

    def postorder(self, root):
        if root is not None:
            self.postorder(root.left)
            self.postorder(root.right)
            print(root.data)

    def getVerticalOrder(self, node, hDist, map):
        if node is None:
            return
        if hDist in map:
            map[hDist].append(node.data)
        else:
            map[hDist] = [node.data]
        self.getVerticalOrder(node.left, hDist-1, map)
        self.getVerticalOrder(node.right, hDist+1, map)

    def verticalPrint(self, root):
        map = {}
        self.getVerticalOrder(root,0,map)
        print(map)
        for index, value in enumerate(sorted(map)):
            print(map[value])
            for key in map[value]:
                print(key),


tree = Btree()
tree.insert(1)
tree.insert(2)
tree.insert(3)
tree.insert(4)
tree.insert(5)
tree.insert(6)
tree.insert(7)
tree.insert(8)
tree.insert(9)
print("-------")
# tree.inorder(tree.root)
print("-------")
# tree.delete(3)
# tree.delete(5)
print("-------")
# tree.inorder(tree.root)
print("-------")
# print(tree.root.data)
print("-------")
tree.verticalPrint(tree.root)
